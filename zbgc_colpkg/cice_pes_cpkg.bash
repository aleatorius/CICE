NTASK=12 
NPX=1 
NPY=12 
BPX=1
BPY=1
RES=arctic-4km 

MXBLCKS=1
TRAGE=1
TRFY=1
TRPND=1
NTRAERO=0         # number of aerosol tracers
TRBRI=1           # brine height tracer
TRZS=0            # set to 1 for zsalinity tracer
NBGCLYR=7         # number of bio grid layers
TRBGCS=0          #  number of BGC tracers
TRBGCZ=1          # set to 1 for zbgc tracers
TRZAERO=3         # number of z aerosol tracers 
                  # (up to max_aero = 6)
TRALG=3           # number of algal tracers
                  # (up to max_algae = 3)
TRDOC=2           # number of dissolve organic carbon 
                  # (up to max_doc = 3)
TRDIC=0           # number of dissolve inorganic carbon 
                  # (up to max_dic = 1)
TRDON=1           # number of dissolve organic nitrogen
                  # (up to max_don = 1)
TRFEP=1           # number of particulate iron tracers 
                  # (up to max_fe  = 2) 
TRFED=1   
THRD=no
if [ no == 'yes' ]; then  
    OMP_NUM_THREADS=2 ; 
fi # positive integer 
